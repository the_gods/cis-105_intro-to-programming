const userInput = require('readline').createInterface({
    input: process.stdin, // stdin: the keyboard
    output: process.stdout // stdout: the screen
})

userInput.question('What is your name? ', name => {
    console.log("Hello there " + name);

    userInput.question('What is your age? ', age => {
        console.log('Your age is: ' + age);
        userInput.close();
    })
})
