const userInput = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})

userInput.question('Please enter a whole number: ', number => {
    if( (number % 2) == 0) {
        console.log(`${number} is even`);
    } else {
        console.log(`${number} is odd`);
    }
    userInput.close();
});
